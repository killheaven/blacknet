/*
 * Copyright (c) 2018 Pavel Vasin
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

package ninja.blacknet.packet

import kotlinx.io.core.ByteReadPacket
import kotlinx.serialization.Serializable
import mu.KotlinLogging
import ninja.blacknet.db.PeerDB
import ninja.blacknet.network.*
import ninja.blacknet.serialization.BinaryEncoder
import kotlin.random.Random

private val logger = KotlinLogging.logger {}

@Serializable
internal class Version(
        private val magic: Int,
        private val version: Int,
        private val time: Long,
        private val nonce: Long,
        private val agent: String,
        private val feeFilter: Long,
        private val chain: ChainAnnounce
) : Packet {
    override fun serialize(): ByteReadPacket = BinaryEncoder.toPacket(serializer(), this)

    override fun getType() = PacketType.Version

    override suspend fun process(connection: Connection) {
        if (magic != Node.magic) {
            connection.close()
            return
        }

        connection.timeOffset = Runtime.time() - time
        connection.peerId = Node.newPeerId()
        connection.version = version
        connection.agent = Bip14.sanitize(agent)
        connection.feeFilter = feeFilter
        connection.lastChain = chain

        if (version < Node.minVersion) {
            logger.info("${connection.debugName()} obsolete version $agent")
            connection.close()
            return
        }

        if (connection.state == Connection.State.INCOMING_WAITING) {
            if (nonce == Node.nonce) {
                connection.close()
                return
            }
            if (version >= FIXED_NONCE_VERSION)
                Node.sendVersion(connection, nonce)
            else
                Node.sendVersion(connection, Random.nextLong())
            connection.state = Connection.State.INCOMING_CONNECTED
            logger.info("Accepted connection from ${connection.debugName()} $agent")
        } else {
            connection.state = Connection.State.OUTGOING_CONNECTED
            PeerDB.connected(connection.remoteAddress)
            if (PeerDB.isLow())
                connection.sendPacket(GetPeers())
            logger.info("Connected to ${connection.debugName()} $agent")
        }

        ChainFetcher.offer(connection, chain.chain, chain.cumulativeDifficulty)
    }

    companion object {
        const val FIXED_NONCE_VERSION = 7
    }
}
